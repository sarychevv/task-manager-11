package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
