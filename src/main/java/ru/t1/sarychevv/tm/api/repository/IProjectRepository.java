package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    List<Project> findAll();

    void clear();

    Project create(String name, String description);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

}
