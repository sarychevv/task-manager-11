package ru.t1.sarychevv.tm.api.controller;

public interface IProjectController {

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
