package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
